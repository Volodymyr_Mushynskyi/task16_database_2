use labor_sql;

-- 4
select maker,type from product
where type in (select type from product where type="PC");

select maker,type from product
where type <> all (select type from product where type="laptop");

select maker,type from product
where type=any (select type from product where type<>"laptop");

select distinct maker from product
where type in (select type from product where type="PC" or type="laptop");

select distinct maker,type from product
where type<> all (select type from product where type="Printer");

select distinct maker,type from product
where type= any(select type from product where type<>"Printer");


-- 5
select maker,model,type from product a
where exists (select model from pc where a.model = model);

select maker,model,type from product a
where exists (select model from pc where a.model = model and speed>=750);

select maker,model,type from product a
where exists (select model from pc where a.model = model and speed>=750) 
or exists (select model from laptop where a.model = model and speed>=750);

select model, max(speed) from pc a
where exists (select model from product where a.model = model and type="Printer");

select class, displacement,year from classes,(select launched as year, name as n from ships where launched>1922) as ll
where displacement>35000 and class = n;

-- 6

select 'середня ціна=' as text, avg(price) as price from pc;

select 'модель=',model,'швидкість=',speed,'оперативна пам*ять=',ram,'жосткий диск=',hd,'ціна=',price from pc;

select date(date) from income;

select ship,battle,REPLACE(REPLACE(REPLACE(result, 'sunk', 'Потонув'), 'OK', 'Цілий'),'damaged','Підтоплений') as 'Стан' from outcomes;

-- 7
select model, max(price) from printer
group by model;

select model,speed from laptop
where laptop.speed< any (select speed from pc);

select model,price from printer
where type="Jet"
group by model
having min(price);

select (select distinct maker as mak from product), count(model) from pc 
group by maker limit 2;

-- 8
select maker,
(select count(type) from product where type="PC" and maker=P.maker) PC,
(select count(type) from product where type="Laptop" and maker=P.maker) Laptop,
(select count(type) from product where type="Printer" and maker=P.maker) Printer
from product P
group by maker;

select maker,
(select avg(screen) from Laptop where  model=P.model) Laptop
from Product P
group by maker;

select maker,
(select max(price) from PC where  model=P.model) price
from Product P
group by maker;

select maker,
(select min(price) from PC where  model=P.model) price
from Product P
group by maker;

select model,speed,
(select avg(price) from PC where speed>=600 and speed=P.speed and model=P.model) price
from PC P;

